/*
** parfait.c for  in /home/le-que_v/rendu/dante/generateur
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon May  2 16:31:56 2016 Vincent Le Quec
** Last update Fri May 27 16:56:53 2016 Vincent Le Quec
*/

#include "generateur.h"

void		epur_grille(t_labyrinth *lab)
{
  int		i;
  int		j;

  i = 0;
  j = lab->width - 2;
  while (lab->tab[i])
    {
      if (lab->tab[i][j] == 'X' && lab->tab[i][j - 1] == '*' && i % 3 == 0)
	lab->tab[i][j] = '*';
      i++;
    }
  j = 0;
  i--;
  while (lab->tab[i][j])
    {
      if (lab->tab[i][j] == 'X' && lab->tab[i - 1][j] == '*' && j % 3 == 0)
	lab->tab[i][j] = '*';
      j++;
    }
}

void		make_end(t_labyrinth *lab)
{
  int		i;
  int		j;

  i = 0;
  j = 0;
  while (lab->tab[i])
    {
      while (lab->tab[i][j])
	j++;
      i++;
    }
  i--;
  j--;
  if (lab->tab[i][j] == 'X')
    {
      lab->tab[i][j] = '*';
      if (lab->tab[i - 1][j] == 'X')
	lab->tab[i - 1][j] = '*';
      else
	if (lab->tab[i][j - 1] == 'X')
	  lab->tab[i][j - 1] = '*';
    }
}

int		perfect(t_labyrinth *lab)
{
  labyrinth(lab);
  epur_tab(lab);
  epur_grille(lab);
  make_end(lab);
  display_tab(lab);
  free_tab(lab);
  free(lab);
  return (0);
}
