/*
** check_position.c for check_position in /home/pellet_r/CPE/dante/generateur
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Wed May  4 12:56:24 2016 remy PELLETIER
** Last update Fri May 27 16:56:30 2016 Vincent Le Quec
*/

#include "generateur.h"

int             check_left(t_labyrinth *lab, int i, int j, char c)
{
  if (c == '0')
    {
      if (j <= 1)
        return (0);
      if (lab->tab[i][j - 2] == c)
        return (4);
    }
  else
    {
      if (j <= 0)
        return (0);
      if (lab->tab[i][j - 1] == c)
        return (4);
    }
  return (0);
}

int             check_right(t_labyrinth *lab, int i, int j, char c)
{
  if (c == '0')
    {
      if (j >= lab->width - 2)
        return (0);
      if (lab->tab[i][j + 2] == c)
        return (2);
    }
  else
    {
      if (j >= lab->width - 1)
        return (0);
      if (lab->tab[i][j + 1] == c)
        return (2);
    }
  return (0);
}

int             check_top(t_labyrinth *lab, int i, int j, char c)
{
  if (c == '0')
    {
      if (i <= 1)
        return (0);
      if (lab->tab[i - 2][j] == c)
        return (1);
    }
  else
    {
      if (i == 0)
        return (0);
      if (lab->tab[i - 1][j] == c)
        return (1);
    }
  return (0);
}

int             check_bottom(t_labyrinth *lab, int i, int j, char c)
{
  if (c == '0')
    {
      if (i >= lab->height - 3)
        return (0);
      if (lab->tab[i + 2][j] == c)
        return (3);
    }
  else
    {
      if (i >= lab->height - 2)
        return (0);
      if (lab->tab[i + 1][j] == c)
        return (3);
    }
  return (0);
}
