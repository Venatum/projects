/*
** main.c for  in /home/le-que_v/rendu/dante/generateur
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon May  2 16:34:20 2016 Vincent Le Quec
** Last update Fri May 27 16:56:44 2016 Vincent Le Quec
*/

#include "generateur.h"

int		main(int ac, char **av)
{
  t_labyrinth   *lab;

  if (ac == 4)
    {
      srand(getpid());
      if ((lab = malloc(sizeof(*lab))) == NULL)
	my_perror("malloc");
      if ((init_struct(lab, av)) == -1)
	{
	  fprintf(stderr, "Bad parameters\n");
	  return (-1);
	}
      if (!strcmp(av[3], "parfait") || !strcmp(av[3], "perfect"))
	perfect(lab);
      else
	if (!strcmp(av[3], "imparfait") || !strcmp(av[3], "imperfect"))
	  imperfect(lab);
      else
	fprintf(stderr, "Usage: ./generateur x y [parfait or imparfait]\n");
    }
  else
    fprintf(stderr, "Usage: ./generateur x y [parfait or imparfait]\n");
  return (0);
}
