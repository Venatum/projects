/*
** imparfait.h for  in /home/le-que_v/rendu/dante/generateur
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon May  2 16:17:00 2016 Vincent Le Quec
** Last update Fri May 27 16:54:18 2016 Vincent Le Quec
*/

#ifndef		_GENERATEUR_H_
# define	_GENERATEUR_H_

#define HEIGHT_LAB	6
#define WIDTH_LAB	24

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

typedef struct	s_labyrinth
{
  char		**tab;
  int		width;
  int		height;
  int		end;
  int		sup;
  int		res_tab[4];
  int		i;
  int		j;
}		t_labyrinth;

int             imperfect(t_labyrinth *lab);
int             perfect(t_labyrinth *lab);
int             labyrinth(t_labyrinth *lab);
int             what_num(t_labyrinth *lab, int i, int j, char c);

/* Check */
int             check_left(t_labyrinth *lab, int i, int j, char c);
int             check_right(t_labyrinth *lab, int i, int j, char c);
int             check_top(t_labyrinth *lab, int i, int j, char c);
int             check_bottom(t_labyrinth *lab, int i, int j, char c);

/* Tools Laby*/
void            swap_tab(int nb_true, int *nb_choice, t_labyrinth *lab);
int             find_nb_true(int *nb_choice);
void            add_grille(t_labyrinth *lab);
int             create_tab(t_labyrinth *lab);

/* Tools */
void            display_tab(t_labyrinth *lab);
int             init_struct(t_labyrinth *lab, char **av);
void            free_tab(t_labyrinth *lab);
void            epur_tab(t_labyrinth *lab);
int             my_getnbr(char *str);
void            my_perror(char *str);
void		make_end(t_labyrinth *lab);

#endif /* _GENERATEUR_H_ */
