/*
** error.c for  in /home/le-que_v/rendu/dante/astar
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon Apr 25 16:53:37 2016 Vincent Le Quec
** Last update Sat May 28 15:58:27 2016 Vincent Le Quec
*/

#include "solver.h"

void            my_perror(char *str)
{
  perror(str);
  exit(1);
}
