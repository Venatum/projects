/*
** labyrinth.h for labyrinth in 
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Mon Apr 25 11:01:55 2016 remy PELLETIER
** Last update Tue May 17 15:43:14 2016 Vincent Le Quec
*/

#ifndef		_LABYRINTH_H_
# define	_LABYRINTH_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "color.h"

#define SECURE		10

typedef struct	s_labyrinth
{
  char		**tab;
  int		**pos_parent;
  int		*pos;
  int		width;
  int		height;
  int		i;
  int		j;
  int		index_en_cour;
  int		index_parent;
  int		end_i;
  int		end_j;
}		t_labyrinth;

ssize_t		getline(char **lineptr, size_t *n, FILE *stream);

/*algo*/
void            find_possibilite(t_labyrinth *lab, int pos, int *tab_possib);
int             algo(t_labyrinth *lab, int pos);
int             largeur(t_labyrinth *lab, char **av);

/*check_position*/
int             check_left(t_labyrinth *lab, int i, int j);
int             check_right(t_labyrinth *lab, int i, int j);
int             check_top(t_labyrinth *lab, int i, int j);
int             check_bottom(t_labyrinth *lab, int i, int j);
int             what_position(t_labyrinth *lab, int pos, int nb_alea);

/*create_tab*/
int             fill_tab(t_labyrinth *lab, char **av);
int             create_tab(t_labyrinth *lab);
int             create_tab_parent(t_labyrinth *lab);

/*display*/
void            display_tab_color(char **tab);
void            display_path(t_labyrinth *lab);

/*find_position*/
int             find_nb_true(int *nb_choice);
int             *swap_tab(int *nb_choice, int *res_tab);
int             *what_num(t_labyrinth *lab, int i, int j);

/*init_struct*/
int             calc_width_height(t_labyrinth *lab, char **av);
int             init_struct(t_labyrinth *lab, char **av);

/*tools*/
void            display_tab(t_labyrinth *lab);
void            free_tab_char(t_labyrinth *lab);
void            free_tab_int(t_labyrinth *lab);
void            epur_tab(t_labyrinth *lab);
void            epur_tab(t_labyrinth *lab);
void            my_perror(char *str);

#endif		/*LABYRINTH*/
