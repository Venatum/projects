/*
** init_struct.c for init_struct in /home/pellet_r/CPE/dante/tmp_largeur/old
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Tue May 17 12:07:01 2016 remy PELLETIER
** Last update Wed May 18 12:49:34 2016 Vincent Le Quec
*/

#include "labyrinth.h"

char		find_bad_char(char *buff)
{
  int		i;

  i = 0;
  while (buff[i])
    {
      if (buff[i] != '*' && buff[i] != 'X' && buff[i] != '\n')
	return (buff[i]);
      i++;
    }
  return ('*');
}

int		calc_width_height2(char		*buff,
				   FILE		*fd,
				   t_labyrinth	*lab,
				   size_t	size)
{
  int		width_check;
  char		c;

  width_check = 0;
  while ((getline(&buff, &size, fd)) != -1)
    {
      if ((c = find_bad_char(buff)) != '*')
	{
	  free(buff);
	  fprintf(stderr, "Bad charactere expected [%c]\n", c);
	  return (-3);
	}
      lab->width = strlen(buff);
      if (width_check == 0)
	width_check = lab->width;
      if (width_check != lab->width)
	{
	  free(buff);
	  return (-1);
	}
      lab->height += 1;
    }
  free(buff);
  return (0);
}
int             calc_width_height(t_labyrinth *lab, char **av)
{
  FILE          *fd;
  char          *buff;
  int		check;
  size_t	size;

  size = 0;
  fd = NULL;
  buff = NULL;
  if ((fd = fopen(av[1], "r+")) == NULL)
    my_perror("fopen");
  check = calc_width_height2(buff, fd, lab, size);
  fclose(fd);
  return (check);
}

int             init_struct(t_labyrinth *lab, char **av)
{
  int		check;

  lab->tab = NULL;
  lab->pos_parent = NULL;
  lab->width = 0;
  lab->height = 0;
  if ((check = calc_width_height(lab, av)) != 0)
    return (check);
  lab->index_parent = 0;
  lab->index_en_cour = 0;
  lab->end_i = lab->height - 1;
  lab->end_j = lab->width - 2;
  lab->i = 0;
  lab->j = 0;
  if ((lab->pos = malloc(sizeof(*lab->pos) * 2)) == NULL)
    my_perror("malloc");
  if (lab->width <= 0 || lab->height <= 0)
    return (-1);
  return (0);
}
