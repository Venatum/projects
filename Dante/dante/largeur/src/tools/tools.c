/*
** tools.c for tools in /home/pellet_r/CPE/dante/tmp_largeur/old
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Tue May 17 11:49:58 2016 remy PELLETIER
** Last update Tue May 17 15:49:04 2016 Vincent Le Quec
*/

#include "labyrinth.h"

void		my_perror(char *str)
{
  perror(str);
  exit(1);
}

void            display_tab(t_labyrinth *lab)
{
  int           i;

  i = 0;
  while (lab->tab[i])
    printf("%s\n", lab->tab[i++]);
}

void            free_tab_char(t_labyrinth *lab)
{
  int           i;

  i = 0;
  while (lab->tab[i])
    free(lab->tab[i++]);
  free(lab->tab);
}

void            free_tab_int(t_labyrinth *lab)
{
  int           i;

  i = 0;
  while (lab->pos_parent[i])
    free(lab->pos_parent[i++]);
  free(lab->pos_parent);
}

void            epur_tab(t_labyrinth *lab)
{
  int           i;
  int           j;

  j = 0;
  i = 0;
  while (lab->tab[i])
    {
      j = 0;
      while (lab->tab[i][j])
        {
          if (lab->tab[i][j] == '#' || lab->tab[i][j] == 'A')
            lab->tab[i][j] = '*';
          if (lab->tab[i][j] == 'Q')
            lab->tab[i][j] = 'o';
          j++;
        }
      i++;
    }
}
