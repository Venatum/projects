/*
** find_position.c for find_position in /home/pellet_r/CPE/dante/tmp_largeur/old
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Tue May 17 11:54:14 2016 remy PELLETIER
** Last update Tue May 17 15:33:52 2016 Vincent Le Quec
*/

#include "labyrinth.h"

int             find_nb_true(int *nb_choice)
{
  int           i;
  int           nb_true;

  i = 0;
  nb_true = 0;
  while (i < 4)
    if (nb_choice[i++] != 0)
      nb_true++;
  return (nb_true);
}

int             *swap_tab(int *nb_choice, int *res_tab)
{
  int           i;
  int           k;

  k = 0;
  i = 0;
  if ((res_tab = malloc(sizeof(int) * SECURE)) == NULL)
    my_perror("malloc");
  while (i < 4)
    {
      if (nb_choice[i] != 0)
        {
          res_tab[k] = nb_choice[i];
          k++;
        }
      res_tab[k] = 0;
      i++;
    }
  return (res_tab);
}

int             *what_num(t_labyrinth *lab, int i, int j)
{
  int           nb_choice[4];
  int           nb_true;
  int           *res_tab;

  res_tab = NULL;
  nb_choice[0] = check_left(lab, i, j);
  nb_choice[1] = check_right(lab, i, j);
  nb_choice[2] = check_top(lab, i, j);
  nb_choice[3] = check_bottom(lab, i, j);
  if ((nb_true = find_nb_true(nb_choice)) == 0)
    return (0);
  res_tab = swap_tab(nb_choice, res_tab);
  return (res_tab);
}
