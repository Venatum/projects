#!/bin/sh
## test.sh for  in /home/le-que_v/rendu/dante
## 
## Made by Vincent Le Quec
## Login   <le-que_v@epitech.net>
## 
## Started on  Tue May 10 10:15:16 2016 Vincent Le Quec
## Last update Fri May 13 11:10:13 2016 Vincent Le Quec
##

FLAGS="$1"
BINARY="$2"

# Flags Help
if [ "$FLAGS" == "--help" ] ||
    [ "$FLAGS" == "-h" ] ||
    [ "$FLAGS"  == "" ]
then
    echo "Usage: ./perf.sh Flags Binary < [Map] | [width] [height] [parfait | imparfait] >">&2
    exit -1
fi

# Solver
if [ "$BINARY" == "astar/solver" ] ||
    [ "$BINARY" == "largeur/solver" ] ||
    [ "$BINARY" == "profondeur/solver" ]
then
    MAP="$3"
    echo "OK: Binary is $BINARY"
    echo ""
    if [ -f "$MAP" ];
    then
	echo "OK: Map is $MAP."
	if [ "$FLAGS" == "time" ]
	then
	    time ./"$BINARY" "$MAP"
	else
	    "$FLAGS" ./"$BINARY" "$MAP"
	fi
    else
	echo "KO: Error Map -> $MAP.">&2
	exit -1
    fi
fi

# Generateur
if [ "$BINARY" == "generateur/generateur" ]
then
    X="$3"
    Y="$4"
    OPT="$5"
    echo "OK: Binary is $BINARY"
    echo ""
    if [ "$FLAGS" == "time" ]
    then
	time ./"$BINARY" "$X" "$Y" "$OPT"
    else
	"$FLAGS" ./"$BINARY" "$X" "$Y" "$OPT"
    fi
fi

# Other Binary
if [ "$BINARY" != "generateur/generateur" ] &&
    [ "$BINARY" == "astar/solver" ] &&
    [ "$BINARY" == "largeur/solver" ] &&
    [ "$BINARY" == "profondeur/solver" ]
then
    echo "KO: Binary <solver or generateur>"
fi
