/*
** check.c for  in /home/le-que_v/rendu/dante/profondeur
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon May 16 12:58:57 2016 Vincent Le Quec
** Last update Sat May 21 14:55:46 2016 Vincent Le Quec
*/

#include "profondeur.h"

int             check_left(t_labyrinth *lab, int i, int j, char c)
{
  if (j <= 0)
    return (0);
  if (lab->tab[i][j - 1] == c)
    return (4);
  return (0);
}

int             check_right(t_labyrinth *lab, int i, int j, char c)
{
  if (j >= lab->width - 1)
    return (0);
  if (lab->tab[i][j + 1] == c)
    return (2);
  return (0);
}
int             check_top(t_labyrinth *lab, int i, int j, char c)
{
  if (i == 0)
    return (0);
  if (lab->tab[i - 1][j] == c)
    return (1);
  return (0);
}

int             check_bottom(t_labyrinth *lab, int i, int j, char c)
{
  if (i >= lab->height - 2)
    return (0);
  if (lab->tab[i + 1][j] == c)
    return (3);
  return (0);
}

int             what_num(t_labyrinth *lab, int i, int j, char c)
{
  int           nb_choice[4];
  int           res;
  int		index;

  index = 0;
  res = 0;
  nb_choice[0] = check_right(lab, i, j, c);
  nb_choice[1] = check_bottom(lab, i, j, c);
  nb_choice[2] = check_top(lab, i, j, c);
  nb_choice[3] = check_left(lab, i, j, c);
  while (index < 4)
    {
      if (nb_choice[index] != 0)
	{
	  res = nb_choice[index];
	  index = 3;
	}
      index++;
    }
  return (res);
}
