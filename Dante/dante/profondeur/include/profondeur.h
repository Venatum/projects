/*
** profondeur.h for  in /home/le-que_v/rendu/dante/profondeur
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Sat May 21 14:54:34 2016 Vincent Le Quec
** Last update Sat May 21 14:54:53 2016 Vincent Le Quec
*/

#ifndef		_PROFONDEUR_H_
# define	_PROFONDEUR_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

typedef struct	s_labyrinth
{
  char		**tab;
  int		width;
  int		height;
  int		i;
  int		j;
  int		end_i;
  int		end_j;
}		t_labyrinth;

ssize_t		getline(char **lineptr, size_t *n, FILE *stream);
int             find_nb_true(int *nb_choice);
int		labyrinth(t_labyrinth *lab, char **av);

/*Check*/
int		check_begin(t_labyrinth *lab);
int             check_left(t_labyrinth *lab, int i, int j, char c);
int             check_right(t_labyrinth *lab, int i, int j, char c);
int             check_top(t_labyrinth *lab, int i, int j, char c);
int             check_bottom(t_labyrinth *lab, int i, int j, char c);
int             what_num(t_labyrinth *lab, int i, int j, char c);

/*Tab*/
int             create_tab(t_labyrinth *lab);
void            swap_tab(int *nb_choice, t_labyrinth *lab);
int             fill_tab(t_labyrinth *lab, char **av);

/*Tools*/
int             init_struct(t_labyrinth *lab, char **av);
void            epur_tab(t_labyrinth *lab);
void            display_tab(t_labyrinth *lab);
void            free_tab(t_labyrinth *lab);
int		my_getnbr(char *);

/*Error*/
void		my_perror(char *str);

#endif		/* _PROFONDEUR_H_ */
